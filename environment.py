import numpy as np
import pandas as pd
import random
from components import RW, MTQ, MagnetSensor, SS, Gyro
from obc import OBC
from setting import Setting


class Environment(object):
    def __init__(self, Setting):
        self.time_step = Setting.time_step
        self.lat = Setting.init_lat
        self.long = Setting.init_long
        self.altitude = Setting.init_altitude
        self.sun_dir_i = Setting.init_sun_direct
        self.tesla_i = self.calc_tesla()

    def calc_tesla(self):
        """
        :return: tesla is 3-dim variable. 1: North, 2:East, 3:Down direction of magnetic field B
        """
        def c(x):
            return np.cos(x)
        def s(x):
            return np.sin(x)
        p = self.lat
        q = self.long
        tesla = (6278.0/self.altitude)**3*np.array([[  -c(p),  s(p)*c(q),      s(p)*s(q)],
                                                    [      0,      s(q),           -c(q)],
                                                    [-2*s(p), -2*c(p)*c(q), -2*c(p)*s(q)]])\
                .dot(np.array([[-29900, -1900, 5530]]).T)
        return tesla
