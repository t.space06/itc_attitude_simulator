import numpy as np
import pandas as pd
import random
from components import RW, MTQ, MagnetSensor, SS, Gyro
from obc import OBC
from setting import Setting


class Dynamics(object):
    def __init__(self, Setting):
        self.time_step = Setting.time_step
        self.quaternion = Setting.init_quaternion
        self.ang_vel = Setting.init_ang_vel
        self.inertia = Setting.inertia #Only main inertia axis
        self.time = 0
        self.data_history = {}

    def tick(self):
        self.time = self.time + self.time_step

    def make_recorder(self):
        for key, value in self.__dict__.items():
            print(key)
            self.data_history.update({key: [value]})

    def record(self):
        for key, value in self.__dict__.items():
            try:
                self.data_history[key].append(value)
            except:
                print("Recorder instance is not created")

    def propagate(self, torque):
        dt = self.time_step
        k0_q, k0_a  = att_change(self.inertia, self.quaternion, self.ang_vel, torque)
        k1_q, k1_a = att_change(self.inertia, self.quaternion + k0_q*dt/2.0, self.ang_vel + k0_a*dt/2.0, torque)
        k2_q, k2_a = att_change(self.inertia, self.quaternion + k1_q*dt/2.0, self.ang_vel + k1_a*dt/2.0, torque)
        k3_q, k3_a = att_change(self.inertia, self.quaternion + k2_q*dt, self.ang_vel + k2_a*dt, torque)
        self.quaternion = self.quaternion + 1.0 / 6.0*(k0_q + 2*k1_q + 2*k2_q + k3_q)*dt
        self.ang_vel = self.ang_vel + 1.0 / 6.0 * (k0_a + 2 * k1_a + 2 * k2_a + k3_a)*dt


def att_change(Inertia, quaternion, ang_vel, torque):
    d_ang_vel = np.copy(ang_vel)
    d_ang_vel[0] = (torque[0]+(Inertia[1]-Inertia[2])*ang_vel[1]*ang_vel[2])/Inertia[0]
    d_ang_vel[1] = (torque[1] + (Inertia[2] - Inertia[0]) * ang_vel[2] * ang_vel[0]) / Inertia[1]
    d_ang_vel[2] = (torque[2] + (Inertia[0] - Inertia[1]) * ang_vel[0] * ang_vel[1]) / Inertia[2]
    d_quaternion = omega(ang_vel).dot(quaternion)
    return d_quaternion, d_ang_vel

def omega(x) :
    omega = 1.0/2.0*np.array([[    0,   x[2], -x[1],  x[0]],
                              [-x[2],      0,  x[0],  x[1]],
                              [ x[1],  -x[0],     0,  x[2]],
                              [-x[0],  -x[1], -x[2],  0]])
    return omega


