import numpy as np
import random
from numpy import linalg as LA
from components import Component

class OBC(Component):
    def __init__(self, Setting):
        super().__init__(Setting.time_step)
        self.calculation_period = Setting.obc_calculation_period
        self.mag_flux = np.zeros([3, 1])
        self.ang_vel_gyro = np.zeros([3, 1])
        self.sun_dir = np.zeros([3, 1])
        self.attitude = np.array([0, 0, 0, 1])
        self.ang_vel = np.zeros([3, 1])
        self.torque_rw_dir = np.zeros([3, 1])
        self.mag_dir = np.zeros([3, 1])
        self.torque_rw_dir = np.zeros([3, 1])
        self.mtq_dir = np.zeros([3, 1])
        self.target_attitude = np.array([0, 0, 0, 1])
        self.target_angular_vel = np.array([0, 0, 0])
        self.ctrl_mode = "PID"

    def get_info(self, magnet_sensor, ss, gyro):
        self.mag_flux = magnet_sensor.output()
        self.ang_vel_gyro = gyro.output()
        self.sun_dir = ss.output()

    def calc_attiude(self, envir_info):
        """
        Clac quertanion from mag_flux, ang_vel, sun_dir
        :return:
        """

        self.attitude = np.zeros([4, 1])

    def set_target_attitude(self, quertanion):
        self.target_attitude = quertanion

    def set_target_angular_vel(self,  ang_vel):
        self.target_angular_vel = ang_vel

    def set_target_att_and_ang_vel(self, quertanion, ang_vel):
        self.target_attitude = quertanion
        self.target_angular_vel = ang_vel

    def change_ctrl_mode(self, mode):
        if not mode in ["PID", "Bdot", "Unloading"]:
            print("Command is invalid")
            return 0
        self.ctrl_mode = mode

    def control_attitude(self):
        if self.ctrl_mode =="PID":
            self.torque_rw_dir = A*(self.target_attitude-self.attitude)+B*(self.target_angular_vel-self.ang_vel)
            self.direct_mtq = np.zeros([3,1])
        elif self.ctrl_mode == 'Bdot':
            self.mtq_dir = np.zeros([3,1])

    def torque_rw(self):
        return self.torque_rw_dir

    def direct_mtq(self):
        return self.mtq_dir
