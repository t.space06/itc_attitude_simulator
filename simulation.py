import numpy as np
import random
from numpy import linalg as LA
import matplotlib.pyplot as plt
import os
from setting import *
from components import *
from setting import Setting
from satellite import Satellite
from dynamics import Dynamics
from environment import Environment




class SIMULATION(object):
    def __init__(self, Setting, save_path):
        self.Satellite = Satellite(Setting)
        self.Dynamics = Dynamics(Setting)
        self.Environment = Environment(Setting)
        self.total_record_time = Setting.total_record_time
        self.save_path = save_path
        self.time_step = Setting.time_step

    def run(self, command_dict):
        """
        Assume command_dict has following list
        1L Command Time, 2: Command Name, 3: Command args

        """
        time = 0
        self.Satellite.make_recorder()
        self.Dynamics.make_recorder()
        while time < self.total_record_time:
            for key in command_dict.keys():
                if round(time, 3) == float(key):
                    eval('self.Satellite.' + command_dict[key]['command'] + '(' + str(command_dict[key]['args']) + ')')
                    # command herer;
            self.Satellite.get_info(self.Dynamics, self.Environment)
            self.Satellite.calc_attitude(self.Environment)
            self.Satellite.set_target_torque()
            torque = self.Satellite.generate_torque(self.Environment)
            self.Dynamics.propagate(torque)
            self.Satellite.tick()
            self.Satellite.record()
            self.Dynamics.tick()
            self.Dynamics.record()
            time = time + self.time_step

    def visualize_result_satellite(self, vis_val):
        for key in vis_val:
            if 'obc.' in key:
                Telem = self.Satellite.obc.data_history[key.replace('obc.','')]
            for i in range(0,len(self.Satellite.rw)):
                if 'rw['+ str(i) +'].' in key:
                    Telem = self.Satellite.rw[i].data_history[key.replace('rw['+str(i)+'].','')]
            for i in range(0,len(self.Satellite.mtq)):
                if 'mtq['+ str(i) +'].' in key:
                    Telem = self.Satellite.mtq[i].data_history[key.replace('mtq.','')]
            if 'magnet_sensor.' in key:
                Telem = self.Satellite.magnet_sensor.data_history[key.replace('magnet_sensor.','')]
            if 'ss.' in key:
                Telem = self.Satellite.ss.data_history[key.replace('ss.','')]
            if 'gyro.' in key:
                Telem = self.Satellite.gyro.data_history[key.replace('gyro.','')]
            if Telem == None:
                print('Iinvalid')

            fig, ax = plt.subplots()
            time_history = np.linspace(0, self.total_record_time, len(Telem))
            plt.title(key)
            dim = 1
            #try:
            dim = len(Telem[0])
            try:
                for i in range(0, dim):
                    values = [elem[i] for elem in Telem]
                    ax.plot(time_history, values, label=str(i))
            except:
                pass
             #   ax.plot(time_history, Telem)
            ax.legend()
            plt.grid(True)
            plt.savefig(self.save_path + '/' + key + '.png')

    def visualize_result_dynamics(self):
        for key in ['quaternion', 'ang_vel']:
            fig, ax = plt.subplots()
            time_history = np.linspace(0, self.total_record_time, len(self.Dynamics.data_history[key]))
            plt.title(key)
            dim = 1
            try:
                dim = len(self.Dynamics.data_history[key][0])
                for i in range(0, dim):
                    values = [elem[i] for elem in self.Dynamics.data_history[key]]
                    ax.plot(time_history, values, label=str(i))
            except:
                ax.plot(time_history, self.Dynamics.data_history[key])
            ax.legend()
            plt.grid(True)
            plt.savefig(self.save_path + '/' + key + '.png')

