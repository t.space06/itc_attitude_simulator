import numpy as np
import random
from numpy import linalg as LA


def rotation_q(q):
    Q = np.array([[q[0]**2+q[1]**2-q[2]**2-q[3]**2, 2*(q[1]*q[2]+q[0]*q[3]), 2*(q[1]*q[3]-q[0]*q[2])],
        					[2*(q[1]*q[2]-q[0]*q[3]), q[0]**2-q[1]**2+q[2]**2-q[3]**2, 2*(q[2]*q[3]+q[0]*q[1])],
        					[2*(q[1]*q[3]+q[0]*q[2]), 2*(q[2]*q[3]-q[0]*q[1]), q[0]**2-q[1]**2-q[2]**2+q[3]**2]])
    return np.squeeze(Q)

class Component(object):
    def __init__(self, time_step):
        self.time = 0
        self.time_step = time_step
        self.data_history = {}

    def make_recorder(self):
        for key, value in self.__dict__.items():
            print(key)
            self.data_history.update({key:[value]})

    def tick(self):
        self.time = self.time + self.time_step

    def record(self):
        for key, value in self.__dict__.items():
            try:
                self.data_history[key].append(value)
            except:
                print("Recorder instance is not created")



class RW(Component):
    def __init__(self, Setting, location):
        super().__init__(Setting.time_step)
        self.max_torque = Setting.rw_max_torque
        self.max_angular = Setting.rw_max_angular
        self.current_angular = Setting.rw_init_angular
        self.control_time_step = Setting.rw_control_time_step
        self.location = location
        self.directed_torque = 0
        self.stored_direct_torque = 0
        self.generated_torque = np.zeros([3,1])

    def set_target(self, direct_torque):
        self.stored_direct_torque = direct_torque

    def rotate_rw(self):
        if round(self.time, 4) % self.control_time_step == 0:
            self.directed_torque = self.stored_direct_torque

        if self.directed_torque > self.max_torque or self.directed_torque < -self.max_torque:
            torque = self.max_torque * np.sign(self.directed_torque)
        else:
            torque = self.directed_torque
        temp_angular = self.current_angular + torque * self.time_step
        if temp_angular > self.max_angular or temp_angular < -self.max_angular:
            self.generated_torque = 0*self.location
            return 0
        self.current_angular = temp_angular
        self.generated_torque = torque*self.location

    def tick(self):
        self.time = self.time + self.time_step
        self.rotate_rw()

    def generate_torque(self):
        return self.generated_torque


class MTQ(Component):
    def __init__(self, Setting, location):
        super().__init__(Setting.time_step)
        self.max_Am = Setting.mtq_max_Am
        self.error_tesla = Setting.mtq_error_tesla
        self.control_time_step = Setting.mtq_control_time_step
        self.current_Am = 0
        self.target_AM = 0
        self.location = location
        self.generated_torque = np.zeros([3,1])

    def set_target(self, direct_Am):
        self.target_AM = direct_Am

    def generate_torque(self, engvir_info):
        if round(self.time,3) %  self.control_time_step == 0:
            self.current_Am = self.target_AM
        self.generated_torque = (self.current_Am * (1+self.error_tesla*np.random.randn())) * engvir_info.tesla_i * self.location
        return self.generated_torque



class MagnetSensor(Component):
    def __init__(self, Setting):
        super().__init__(Setting.time_step)
        self.noize = Setting.mag_noize
        self.input_tesla = np.zeros([3, 1])
        self.output_sense = np.zeros([3, 1])
        self.output_period = Setting.mag_output_period

    def observe(self, dynamic_info, envir_info): #Todo write observation equation
        self.input_tesla = rotation_q(dynamic_info.quaternion).dot(envir_info.tesla_i)


    def output(self):
        if round(self.time,3) % self.output_period == 0:
            self.output_sense = self.input_tesla+np.random.randn(3, 1)*self.noize
        return self.output_sense


class Gyro(Component):
    def __init__(self, Setting):
        super().__init__(Setting.time_step)
        self.max_sense = Setting.gyro_max_sense
        self.noize = Setting.gyro_noize
        self.input_ang_vel = np.zeros([3, 1])
        self.output_sense = np.zeros([3, 1])
        self.output_period = Setting.gyro_output_period

    def observe(self, dynamic_info):
    	self.input_ang_vel = dynamic_info.ang_vel

    def output(self):
        if round(self.time,3) % self.output_period == 0:
            current_sense =  self.input_ang_vel + np.random.randn(3, 1) * self.noize
            lower_limited = [max(-self.max_sense, axis) for axis in current_sense]
            self.output_sense = [min(self.max_sense, axis) for axis in lower_limited]
        return self.output_sense


class SS(Component):
    def __init__(self, Setting):
        super().__init__(Setting.time_step)
        self.noize = Setting.ss_noize
        self.mounted_direction = Setting.ss_mounted_direction
        self.half_width = Setting.ss_half_width
        self.output_sense = np.array([[0, 0, 1]]).T
        self.input_sundir = np.array([[0, 0, 1]]).T
        self.output_period = Setting.ss_output_period

    def observe(self, dynamic_info, envir_info):
        self.input_sundir = rotation_q(dynamic_info.quaternion).dot(envir_info.sun_dir_i)

    def can_be_sensed(self):
        """
        return whether sun is in detectable region of sun sensor
        :param dynamic_info:
        :return:
        """
        for direction in self.mounted_direction:
            i = np.inner(np.squeeze(self.input_sundir), np.squeeze(direction))
            n = LA.norm(self.input_sundir) * LA.norm(direction)
            c = i / n
            deg = np.rad2deg(np.arccos(np.clip(c, -1.0, 1.0)))
            if deg < self.half_width:
                return True
        return False

    def output(self):
        if round(self.time,3) %  self.output_period == 0 and self.can_be_sensed():
            self.output_sense = self.input_sundir + np.random.randn(3, 1) * self.noize
        return self.output_sense

