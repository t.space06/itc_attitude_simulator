import numpy as np
import random
from numpy import linalg as LA
import pandas as pd
import os
import ast

class Setting(object):
    def __init__(self, excel):
        sheet = pd.read_excel(excel, sheet_name = None)
        self.sheet = sheet
        df = sheet['parameter']

        def set_val(val_name):
            return df[df['Variable'] == val_name].loc[:, 'Value'].values[0]

        self.time_step = float(set_val('time_step'))
        self.rw_max_torque = float(set_val('rw_max_torque'))
        self.rw_max_angular = float(set_val('rw_max_angular'))
        self.rw_init_angular = float(set_val('rw_init_angular'))
        self.rw_control_time_step = float(set_val('rw_control_time_step'))
        self.rw_location = ast.literal_eval(set_val('rw_location'))
        self.rw_location = [np.array([one]).T for one in self.rw_location]
        self.mtq_max_Am = float(set_val('mtq_max_Am'))
        self.mtq_error_tesla = float(set_val('mtq_error_tesla'))
        self.mtq_control_time_step = float(set_val('mtq_control_time_step'))
        self.mtq_location = ast.literal_eval(set_val('mtq_location'))
        self.mtq_location = [np.array([one]).T for one in self.mtq_location]
        self.mag_noize = float(set_val('mag_noize'))
        self.mag_output_period = float(set_val('mag_output_period'))
        self.gyro_max_sense = float(set_val('gyro_max_sense'))
        self.gyro_noize = float(set_val('gyro_noize'))
        self.gyro_output_period = float(set_val('gyro_output_period'))
        self.ss_max_sense = float(set_val('SS_max_sense'))
        self.ss_noize = float(set_val('SS_noize'))
        self.ss_mounted_direction = ast.literal_eval(set_val('SS_mounted_direction'))
        self.ss_mounted_direction = [np.array([one]).T for one in self.ss_mounted_direction]
        self.ss_half_width = float(set_val('SS_half_width'))
        self.ss_output_period = float(set_val('SS_output_period'))
        self.obc_calculation_period = float(set_val('obc_calculation_period'))
        self.init_quaternion =  np.array([ast.literal_eval(set_val('init_quaternion'))]).T
        self.init_ang_vel =  np.array([ast.literal_eval(set_val('init_ang_vel'))]).T
        self.inertia =  np.array([ast.literal_eval(set_val('inertia'))]).T
        self.init_lat = float(set_val('init_lat'))
        self.init_long = float(set_val('init_long'))
        self.init_altitude = float(set_val('init_altitude'))
        self.init_sun_direct = np.array([ast.literal_eval(set_val('init_sun_direct'))]).T
        self.total_record_time = float(set_val('total_record_time'))
        self.command_dict = {}
        self.vis_sat_dict = []
        self.set_command_dict()
        self.set_vis_dict()

    def set_command_dict(self):
        d_command = self.sheet['satellite command']
        for i in range(0,len(d_command)):
            try:
                args = float(d_command.iloc[i,2])
                args = str(args)
            except:
                try:
                    args_pre = ast.literal_eval(d_command.iloc[i,2])
                    args = np.array([args_pre]).T

                    args = 'np.array(['+d_command.iloc[i,2]+']).T'
                except:
                    args = d_command.iloc[i,2]
            self.command_dict.update({str(d_command.iloc[i,0]):{'command':d_command.iloc[i,1], 'args':args}})

    def set_vis_dict(self):
        d_vis = self.sheet['visualization command']
        for i in range(0, len(d_vis)):
            self.vis_sat_dict.append(d_vis.iloc[i, 0])


if __name__ == '__main__':
    path = os.getcwd()+ '/setting.xlsx'
    setting = Setting(path)
    print(setting.command_dict)
    print(setting.vis_sat_dict)
