import os
from setting import Setting
from simulation import SIMULATION
from datetime import datetime

if __name__ == '__main__':
    path = os.getcwd() + '/setting.xlsx'
    setting = Setting(path)
    now = datetime.now()
    current_time = now.strftime("%Y_%m_%d_%H_%M_%S")
    try:
        save_root_folder = os.getcwd() + '/Result/'
        os.mkdir(save_root_folder)
    except:
        pass
    save_folder = save_root_folder + current_time
    save_path = save_folder + '/'
    simulation = SIMULATION(setting, save_path)
    c_dict = setting.command_dict
    vis_sat_dict = setting.vis_sat_dict
    simulation.run(c_dict)
    os.mkdir(save_folder)
    simulation.visualize_result_satellite(vis_sat_dict)
    simulation.visualize_result_dynamics()



