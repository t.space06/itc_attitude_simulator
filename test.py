import numpy as np
import random
from numpy import linalg as LA
import matplotlib.pyplot as plt
import os
from setting import*
from components import*


class UNIT_TEST(object):
    def __init__(self, Class_instance, total_record_time, save_path, dt):
        self.Class_instance = Class_instance # Setting is finished outside of this class
        self.total_record_time = total_record_time
        self.save_path = save_path
        self.dt = dt

    def run_test(self, command_dict):
        """
        Assume command_dict has following list
        1L Command Time, 2: Command Name, 3: Command args

        """
        time = 0
        self.Class_instance.make_recorder()
        while time < self.total_record_time:
            for key in command_dict.keys():
                if round(time, 3) == float(key):
                    eval('self.Class_instance.' + command_dict[key]['command'] + '(' + str(command_dict[key]['args'])+ ')')
                    # command herer;
            self.Class_instance.tick()    
            self.Class_instance.record()
            time = time + self.dt

    def visualize_result(self, vis_val):
        for key in vis_val:
            fig, ax = plt.subplots()
            time_history = np.linspace(0, self.total_record_time, len(self.Class_instance.data_history[key]))
            plt.title(key)
            dim = 1
            if isinstance(self.Class_instance.data_history[key][0], list):
                dim = len(self.Class_instance.data_history[key][0])
                for i in range(0,dim):
                    values = [elem[i] for elem in self.Class_instance.data_history[key]]
                    ax.plot(time_history, values, legend = str(i))
            else:
                ax.plot(time_history, self.Class_instance.data_history[key])
            ax.legend()
            plt.grid(True)
            plt.show()
            plt.savefig(self.save_path+'/'+key+'.png')
                
if __name__ == '__main__':
    path = os.getcwd() + '/parameter.csv'
    setting = Setting(path)
    try:
        os.mkdir(os.getcwd()+'/Result')
    except:
        pass
    save_path = os.getcwd()+'/Result'

    # RW test
    rw = RW(setting, setting.rw_location[0])
    rw_test = UNIT_TEST(rw, 100 , save_path, 0.01)
    rw_dir = {10:{'command':'set_target', 'args': 0.02},
              20:{'command':'set_target', 'args':-0.02},
              30:{'command':'set_target', 'args': 0.02}}
    rw_test.run_test(rw_dir)
    rw_test.visualize_result(['current_angular', 'generated_torque'])

    # MTQ test
    mtq = MTQ(setting, setting.mtq_location[0])
    mtq_test = UNIT_TEST(rw, 100, save_path, 0.01)
    mtq_dir = {10: {'command': 'set_target', 'args': 0.02},
              20: {'command': 'set_target', 'args': -0.02},
              30: {'command': 'set_target', 'args': 0.02}}
    mtq_test.run_test(rw_dir)
    mtq_test.visualize_result(['current_Am', 'generated_torque'])

    # Magnet sensor

    #



