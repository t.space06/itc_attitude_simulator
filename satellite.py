import numpy as np
import pandas as pd
import random
from components import RW, MTQ, MagnetSensor, SS, Gyro
from obc import OBC

class Satellite(object):
    def __init__(self, Setting):
        self.rw = [RW(Setting, Setting.rw_location[0]), RW(Setting, Setting.rw_location[1]), RW(Setting, Setting.rw_location[2])]
        self.mtq = [MTQ(Setting, Setting.mtq_location[0]), MTQ(Setting, Setting.mtq_location[1]), MTQ(Setting, Setting.mtq_location[2])]
        self.magnet_sensor = MagnetSensor(Setting)
        self.obc = OBC(Setting)
        self.ss = SS(Setting)
        self.gyro = Gyro(Setting)

    def make_recorder(self):
        for i in range(0,len(self.rw)):
            self.rw[i].make_recorder()
        for i in range(0,len(self.mtq)):
            self.mtq[i].make_recorder()
        self.magnet_sensor.make_recorder()
        self.obc.make_recorder()
        self.ss.make_recorder()
        self.gyro.make_recorder()


    def get_info(self, dynamic_info, envir_info):
        self.magnet_sensor.observe(dynamic_info, envir_info)
        self.ss.observe(dynamic_info, envir_info)
        self.gyro.observe(dynamic_info)

    def calc_attitude(self, envir_info):
        self.obc.get_info(self.magnet_sensor, self.ss, self.gyro)
        self.obc.calc_attiude(envir_info)

    def set_target_torque(self):
        rw_torque_direction = self.obc.torque_rw()
        mtq_Am_direction = self.obc.direct_mtq()
        for i in range(0, 3):
            self.rw[i].set_target(rw_torque_direction[i])
        for i in range(0, 3):
            self.mtq[i].set_target(mtq_Am_direction[i])

    def generate_torque(self, env_info):
        rw_torque = np.zeros([3,1])
        mtq_torque = np.zeros([3,1])
        for i in range(0, 3):
            temp_torque = self.rw[i].generate_torque()
            rw_torque += temp_torque
        for i in range(0, 3):
            temp_torque = self.mtq[i].generated_torque
            #print(temp_torque)
        return rw_torque+mtq_torque

    def tick(self):
        for i in range(0, len(self.rw)):
            self.rw[i].tick()
        for i in range(0, len(self.mtq)):
            self.mtq[i].tick()
        self.magnet_sensor.tick()
        self.obc.tick()
        self.ss.tick()
        self.gyro.tick()

    def record(self):
        for i in range(0, len(self.rw)):
            self.rw[i].record()
        for i in range(0, len(self.mtq)):
            self.mtq[i].record()
        self.magnet_sensor.record()
        self.obc.record()
        self.ss.record()
        self.gyro.record()





if __name__ == '__main__':
    print(1)